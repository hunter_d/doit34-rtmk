package cn.doitedu.rtmk.utils;

import cn.doitedu.common.pojo.EventBean;
import cn.doitedu.rtmk.pojo.RuleMetaBean;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.MapStateDescriptor;

public class StateDescriptors {

    public static MapStateDescriptor<String, RuleMetaBean> ruleMetaBroadCastStateDesc = new MapStateDescriptor<>("rule-meta-state", String.class, RuleMetaBean.class);


    public static ListStateDescriptor<EventBean> userEventsStateDesc = new ListStateDescriptor<EventBean>("events",EventBean.class);

}
