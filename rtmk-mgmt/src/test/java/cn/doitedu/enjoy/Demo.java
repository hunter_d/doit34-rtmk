package cn.doitedu.enjoy;

import com.jfinal.template.Engine;
import com.jfinal.template.Template;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Demo {
    public static void main(String[] args) {

        // 定义一个模板（模板中含有我们不变的文本，也含有用模板引擎的指令所表达的可变文本）
        String tempStr2 = "hello 88888  \n" +
                "#for(name:names) \n" +
                "您好: #(for.index)  \n" +
                "#end";

        Engine engine = Engine.use();
        Template template = engine.getTemplateByString(tempStr2);

        HashMap<String, List<String>> data = new HashMap<String, List<String>>();
        data.put("names", Arrays.asList("zz","cc"));

        String s2 = template.renderToString(data);

        System.out.println(s2);



    }
}
