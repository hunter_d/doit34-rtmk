package cn.doitedu.rtmk.mgmt.controller;

import cn.doitedu.rtmk.mgmt.pojo.Student;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping(value = "/xx/hello",method = RequestMethod.GET)
    public String hello(String name){
        return "hello " + name;
    }


    @RequestMapping(value = "/yy/hello",method = RequestMethod.POST)
    public String hello2(@RequestBody Student student){
        System.out.println(student);
        return "hello " + student.getName();
    }


}
