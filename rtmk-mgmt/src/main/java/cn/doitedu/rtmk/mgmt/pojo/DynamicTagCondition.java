package cn.doitedu.rtmk.mgmt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DynamicTagCondition {

    private String event_id;
    private List<PropertyCondition> props;
    private int cnt;
    private long window_start;
    private long window_end;
    private int condition_id;

}
