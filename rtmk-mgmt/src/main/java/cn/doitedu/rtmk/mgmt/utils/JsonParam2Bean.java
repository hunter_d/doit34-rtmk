package cn.doitedu.rtmk.mgmt.utils;

import cn.doitedu.rtmk.mgmt.pojo.DynamicTagCondition;
import cn.doitedu.rtmk.mgmt.pojo.PropertyCondition;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;

public class JsonParam2Bean {

    public static DynamicTagCondition dynamicTagConditionToBean(JSONArray dynamicProfileCondition,int index){
        JSONObject jsonObject = dynamicProfileCondition.getJSONObject(index);
        JSONArray propsArray = jsonObject.getJSONArray("props");

        ArrayList<PropertyCondition> propsList = new ArrayList<>();
        for(int i=0; i< propsArray.size(); i++){
            JSONObject obj = propsArray.getJSONObject(i);
            PropertyCondition propertyCondition = new PropertyCondition();
            propertyCondition.setAttr_name(obj.getString("attr_name"));
            propertyCondition.setOperator(obj.getString("operator"));
            propertyCondition.setCompare_value(obj.getString("compare_value"));
            propsList.add(propertyCondition);
        }

        DynamicTagCondition dynamicTagCondition = new DynamicTagCondition();

        dynamicTagCondition.setEvent_id(jsonObject.getString("event_id"));
        dynamicTagCondition.setCondition_id(jsonObject.getInteger("condition_id"));
        dynamicTagCondition.setProps(propsList);
        dynamicTagCondition.setCnt(jsonObject.getInteger("cnt"));
        dynamicTagCondition.setWindow_start(jsonObject.getLong("window_start"));
        dynamicTagCondition.setWindow_end(jsonObject.getLong("window_end"));

        return dynamicTagCondition;
    }

}
