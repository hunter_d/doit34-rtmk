package cn.doitedu.rtmk.mgmt.service;

import cn.doitedu.rtmk.mgmt.dao.RuleMetaDao;
import com.alibaba.fastjson.JSONObject;
import org.roaringbitmap.RoaringBitmap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;

@Service
public class RuleInstanceService {

    @Autowired
    RuleMetaDao ruleMetaDao;


    public void createRuleInstance(RoaringBitmap crowedBitmap, JSONObject ruleInstanceParams) throws SQLException, IOException {
        String ruleModelId = ruleInstanceParams.getString("rule_model_id");
        String groovyCode = ruleMetaDao.findGroovyCodeByModelId(ruleModelId);

        ruleMetaDao.insertRuleInstanceResources(crowedBitmap,ruleInstanceParams,groovyCode);

    }

}
