package cn.doitedu.rtmk.mgmt.dao;

import cn.doitedu.rtmk.mgmt.pojo.DynamicTagCondition;
import cn.doitedu.rtmk.mgmt.utils.JsonParam2Bean;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.template.Engine;
import com.jfinal.template.Template;
import org.roaringbitmap.RoaringBitmap;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;

import java.sql.*;
import java.util.HashMap;

@Repository
public class DynamicTagHistValueDao {

    Connection dorisConnection;
    Connection mysqlConnection;
    PreparedStatement stmt;
    Statement statement;
    Jedis jedis;

    public DynamicTagHistValueDao() throws SQLException {
        dorisConnection = DriverManager.getConnection("jdbc:mysql://doitedu:9030/dwd", "root", "");
        statement = dorisConnection.createStatement();


        mysqlConnection = DriverManager.getConnection("jdbc:mysql://doitedu:3306/doti34", "root", "root");
        stmt = mysqlConnection.prepareStatement("select dynamic_tag_hist_query_sql from rule_model_resource where rule_model_id = ?");

        jedis = new Jedis("doitedu", 6379);

    }

    public void queryDynamicTagHistValue(JSONObject paramObject , RoaringBitmap crowdBitmap) throws SQLException {

        String ruleInstanceId = paramObject.getString("rule_instance_id");
        JSONArray dynamicProfileCondition = paramObject.getJSONArray("dynamic_profile_conditioin");

        for(int i=0; i< dynamicProfileCondition.size(); i++) {
            DynamicTagCondition dynamicTagCondition = JsonParam2Bean.dynamicTagConditionToBean(dynamicProfileCondition, i);
            int conditionId = dynamicTagCondition.getCondition_id();

            // 去mysql中获取到该策略模型对应的动态画像条件历史值查询sql模板

            stmt.setString(1, paramObject.getString("rule_model_id"));
            ResultSet resultSet = stmt.executeQuery();
            resultSet.next();
            String sqlTemplateStr = resultSet.getString("dynamic_tag_hist_query_sql");


            // 用模板引擎去渲染 sql查询语句
            Engine engine = Engine.use();
            Template template = engine.getTemplateByString(sqlTemplateStr);
            HashMap<String, DynamicTagCondition> data = new HashMap<>();
            data.put("bean", dynamicTagCondition);

            String sql = template.renderToString(data);

            // 将 sql 提交给doris来查询
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()){
                int user_id = rs.getInt("user_id");
                int cnt = rs.getInt("cnt");

                if(crowdBitmap.contains(user_id)) {
                    jedis.set(ruleInstanceId + ":" + user_id + ":" + conditionId, cnt + "");
                }
            }

        }
    }


    public static void main(String[] args) throws SQLException {
        DynamicTagHistValueDao dao = new DynamicTagHistValueDao();

        String param = "{\n" +
                "\t\"rule_model_id\":\"model001\",\n" +
                "\t\"rule_instance_id\":\"rule001\",\n" +
                "\t\"fire_condition\":{\n" +
                "\t\t\"event_id\":\"A\",\n" +
                "\t\t\"props\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"attr_name\":\"p1\",\n" +
                "\t\t\t\t\"operator\":\"=\",\n" +
                "\t\t\t\t\"compare_value\": 10\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"attr_name\":\"p3\",\n" +
                "\t\t\t\t\"operator\":\">\",\n" +
                "\t\t\t\t\"compare_value\": 2\n" +
                "\t\t\t},\n" +
                "\t\t]\n" +
                "\t},\n" +
                "\t\"static_profile_condition\":[\n" +
                "\t{\n" +
                "\t\t\"tag_name\":\"tag01\",\n" +
                "\t\t\"operator\":\"match\",\n" +
                "\t\t\"compare_value\": \"咖啡\"\t\t\t\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"tag_name\":\"tag04\",\n" +
                "\t\t\"operator\":\">\",\n" +
                "\t\t\"compare_value\": \"200\"\t\t\t\n" +
                "\t}\n" +
                "\t],\n" +
                "\t\"dynamic_profile_conditioin\":[\n" +
                "\t{\n" +
                "\t\t\"event_id\":\"C\",\n" +
                "\t\t\"props\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"attr_name\":\"p1\",\n" +
                "\t\t\t\t\"operator\":\"=\",\n" +
                "\t\t\t\t\"compare_value\": 2\n" +
                "\t\t\t}\n" +
                "\t\t],\n" +
                "\t\t\"cnt\":2,\n" +
                "\t\t\"window_start\":1670583693000,\n" +
                "\t\t\"window_end\":1670583693000,\n" +
                "\t\t\"condition_id\":1\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"event_id\":\"X\",\n" +
                "\t\t\"props\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"attr_name\":\"p1\",\n" +
                "\t\t\t\t\"operator\":\"=\",\n" +
                "\t\t\t\t\"compare_value\": 1\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"attr_name\":\"p2\",\n" +
                "\t\t\t\t\"operator\":\"=\",\n" +
                "\t\t\t\t\"compare_value\": 2\n" +
                "\t\t\t},\n" +
                "\t\t],\n" +
                "\t\t\"cnt\":2,\n" +
                "\t\t\"window_start\":1670583693000,\n" +
                "\t\t\"window_end\":1670583693000,\n" +
                "\t\t\"condition_id\":2\n" +
                "\t},\n" +
                "\t]\n" +
                "}\n";

        JSONObject jsonObject = JSON.parseObject(param);


        dao.queryDynamicTagHistValue(jsonObject,RoaringBitmap.bitmapOf(1,3,5));


    }



}
