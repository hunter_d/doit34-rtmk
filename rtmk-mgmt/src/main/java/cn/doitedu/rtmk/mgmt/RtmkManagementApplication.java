package cn.doitedu.rtmk.mgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RtmkManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(RtmkManagementApplication.class, args);
    }

}
