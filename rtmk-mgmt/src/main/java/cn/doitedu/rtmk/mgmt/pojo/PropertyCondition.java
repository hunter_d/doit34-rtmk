package cn.doitedu.rtmk.mgmt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropertyCondition {

    private String attr_name;
    private String operator;
    private String compare_value;

}
