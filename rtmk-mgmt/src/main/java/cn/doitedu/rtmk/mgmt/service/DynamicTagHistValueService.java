package cn.doitedu.rtmk.mgmt.service;

import cn.doitedu.rtmk.mgmt.dao.DynamicTagHistValueDao;
import com.alibaba.fastjson.JSONObject;
import org.roaringbitmap.RoaringBitmap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class DynamicTagHistValueService {

    @Autowired
    DynamicTagHistValueDao dynamicTagHistValueDao;


    public void queryDynamicTagHistValue(JSONObject paramObject , RoaringBitmap crowdBitmap) throws SQLException {
        dynamicTagHistValueDao.queryDynamicTagHistValue(paramObject,crowdBitmap);
    }

}
