package cn.doitedu.common.interfaces;

import cn.doitedu.common.pojo.EventBean;
import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.util.Collector;
import org.roaringbitmap.RoaringBitmap;

public interface RuleCalculator {

    public boolean isInitialed();


    public void open(RuntimeContext runtimeContext , JSONObject paramJsonObject, RoaringBitmap roaringBitmap);

    public void processEvent(EventBean eventBean , Collector<String> out);

}
