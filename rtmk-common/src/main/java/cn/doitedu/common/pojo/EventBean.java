package cn.doitedu.common.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventBean {

    int userId;
    String eventId;
    Map<String,String> properties;
    long eventTime;

}
