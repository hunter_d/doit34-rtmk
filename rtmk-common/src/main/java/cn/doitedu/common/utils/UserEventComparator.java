package cn.doitedu.common.utils;

import cn.doitedu.common.pojo.EventBean;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;

@Slf4j
public class UserEventComparator {

    public static boolean userEventIsEqualParam(EventBean userEvent, JSONObject eventParam) throws ParseException {
        String eventIdParam = eventParam.getString("event_id");
        JSONArray attributeParams = eventParam.getJSONArray("props");

        // 2022-09-01 00:00:00
        long windowStart = eventParam.getLong("window_start");
        long windowEnd = eventParam.getLong("window_end");

        if( userEvent.getEventTime()>=windowStart && userEvent.getEventTime()<=windowEnd &&  eventIdParam.equals(userEvent.getEventId())) {
            // 对每一个属性条件进行判断
            for (int j = 0; j < attributeParams.size(); j++) {
                // 取出一个属性参数
                JSONObject attributeParam = attributeParams.getJSONObject(j);

                String paramAttributeName = attributeParam.getString("attr_name");
                String paramCompareType = attributeParam.getString("operator");
                String paramValue = attributeParam.getString("compare_value");

                String eventAttributeValue = userEvent.getProperties().get(paramAttributeName);

                //log.info("比较事件是否匹配条件参数,paramAttributeName:{} , paramCompareType:{} , paramValue:{},eventAttributeValue:{}",paramAttributeName,paramCompareType,paramValue,eventAttributeValue);

                if(eventAttributeValue!=null) {
                    if ("=".equals(paramCompareType) && !(paramValue.compareTo(eventAttributeValue) == 0)) {
                        return false;
                    }

                    if (">".equals(paramCompareType) && !(paramValue.compareTo(eventAttributeValue) > 0)) {
                        return false;
                    }

                    if ("<".equals(paramCompareType) && !(paramValue.compareTo(eventAttributeValue) < 0)) {
                        return false;
                    }

                    if ("<=".equals(paramCompareType) && !(paramValue.compareTo(eventAttributeValue) <= 0)) {
                        return false;
                    }

                    if (">=".equals(paramCompareType) && !(paramValue.compareTo(eventAttributeValue) >= 0)) {
                        return false;
                    }
                }
                log.info("比较事件是否匹配条件参数,paramAttributeName:{} , paramCompareType:{} , paramValue:{},eventAttributeValue:{}",paramAttributeName,paramCompareType,paramValue,eventAttributeValue);
            }
            return true;
        }

        return false;
    }
}
